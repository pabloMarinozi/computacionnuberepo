var AWS = require('aws-sdk');

var handler = function(event, context, callback) {
  var dynamodb = new AWS.DynamoDB({
    apiVersion: '2012-08-10',
    endpoint: 'http://dynamodb:8000',
    region: 'us-west-2',
    credentials: {
      accessKeyId: '2345',
      secretAccessKey: '2345'
    }
  });

  var docClient = new AWS.DynamoDB.DocumentClient({
     apiVersion: '2012-08-10',
     service: dynamodb
  });

  let idEnvio = (event.pathParameters || {}).idEnvio || false;
  switch (event.httpMethod) {
    case "GET":
      switch (event.resource) {
        case "/envios/pendientes":
          // scan indice pendientes
         var params = {
         	TableName: "Envio",
            IndexName: "EnviosPendientesIndex",
         };
         dynamodb.scan(params, function(err, data) {
             if (err) {
               callback(null, {
                 statusCode: 500, body: JSON.stringify(err)
               });
             } else {
               callback(null, {
                 statusCode: 201,
                 body: JSON.stringify(data)
               })
             }
         });
          break;
        case "/envios/{idEnvio}":
          // get por id
          var params = {
              TableName: 'Envio',
              Key: {
                  id: idEnvio,
              },
          };
          docClient.get(params, function(err, data) {
              if (err) {
                callback(null, {
                  statusCode: 500, body: JSON.stringify(err)
                });
              } else {
                callback(null, {
                  statusCode: 201,
                  body: JSON.stringify(data)
                })
              }
          });
          break;
      }
      break;
    case "POST":
      let body = JSON.parse(event.body);
      var item = body;

      switch (event.resource) {
        case "/envios":
          // crear
          item.fechaAlta = new Date().toISOString();
          item.pendiente = item.fechaAlta;
          item.id = guid();

          console.log('item', item);

          docClient.put({
            TableName: 'Envio',
            Item: item
          }, function(err, data) {
            if (err) {
              callback(null, {
                statusCode: 500, body: JSON.stringify(err)
              });
            } else {
              callback(null, {
                statusCode: 201,
                body: JSON.stringify(item)
              })
            }
          });

          break;
        case "/envios/{idEnvio}/movimiento":
          // agregar movimiento
          var movimiento = body;
          // 1) traer por id
          var params = {
              TableName: 'Envio',
              Key: {
                  id: idEnvio,
              },
          };
          docClient.get(params, function(err, data) {
              if (err) {
                callback(null, {
                  statusCode: 500, body: JSON.stringify(err)
                });
              } else {
                var envioRecuperado = data.Item;
                // 2) agregamos historial
                movimiento.fecha = new Date().toISOString();
                console.log('movimiento', movimiento);
                if(envioRecuperado.historial)
                	envioRecuperado.historial.push(movimiento);
                else{
                	envioRecuperado.historial = [movimiento];
                }

                // 3) put para guardar
                docClient.put({
                  TableName: 'Envio',
                  Item: envioRecuperado
                }, function(err, data) {
                  if (err) {
                    callback(null, {
                      statusCode: 500, body: JSON.stringify(err)
                    });
                  } else {
                    callback(null, {
                      statusCode: 201,
                      body: JSON.stringify(movimiento)
                    })
                  }
                });
              }
          });
          break;
        case "/envios/{idEnvio}/entregado":
          // marcar entregado
          // 1) traer por id
          var params = {
              TableName: 'Envio',
              Key: {
                  id: idEnvio,
              },
          };
          docClient.get(params, function(err, data) {
              if (err) {
                callback(null, {
                  statusCode: 500, body: JSON.stringify(err)
                });
              } else {
                var envioRecuperado = data.Item;
                // 2) borrar atributo pendiente
                delete envioRecuperado.pendiente;
                // 3) put para guardar
                docClient.put({
                  TableName: 'Envio',
                  Item: envioRecuperado
                }, function(err, data) {
                  if (err) {
                    callback(null, {
                      statusCode: 500, body: JSON.stringify(err)
                    });
                  } else {
                    callback(null, {
                      statusCode: 201,
                      body: JSON.stringify(envioRecuperado)
                    })
                  }
                });
              }
          });
          
          break;
      }
      break;
    default:
      callback(null, {
        statusCode: 405
      });
  }

}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

exports.handler = handler;
